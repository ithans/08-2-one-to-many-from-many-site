package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.JpaTestBase;
import javafx.scene.Parent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        flushAndClear(em -> {
            ChildEntity childEntity=new ChildEntity("child");
            ParentEntity parentEntity = new ParentEntity("parent");
            childEntity.setParentEntity(parentEntity);
            em.persist(childEntity);
            em.persist(parentEntity);
        });
        run(em ->{
            Assertions.assertNotNull(em.find(ChildEntity.class,1L).getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        flushAndClear(em ->{
            ParentEntity parentEntity = new ParentEntity("parent");
            ChildEntity childEntity = new ChildEntity("child");
            childEntity.setParentEntity(parentEntity);
            em.persist(parentEntity);
            em.persist(childEntity);
        });

        flushAndClear(em->{
            ChildEntity childEntity = em.find(ChildEntity.class, 2L);
            childEntity.setParentEntity(null);
            em.persist(childEntity);
        });
        run(em->{
            Assertions.assertNotNull(em.find(ParentEntity.class,1L));
            Assertions.assertNotNull(em.find(ChildEntity.class,2L));
            Assertions.assertNull(em.find(ChildEntity.class,2L).getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        flushAndClear(em ->{
            ParentEntity parentEntity = new ParentEntity("parent");
            ChildEntity childEntity = new ChildEntity("child");
            childEntity.setParentEntity(parentEntity);
            em.persist(parentEntity);
            em.persist(childEntity);
        });

        flushAndClear(em ->{
            ParentEntity entity = em.find(ParentEntity.class, 1L);
            em.remove(entity);
            ChildEntity child = em.find(ChildEntity.class, 2L);
            em.remove(child);
        });

        run(em->{
            Assertions.assertNull(em.find(ParentEntity.class,1L));
            Assertions.assertNull(em.find(ChildEntity.class,2L));
        });
        // --end-->
    }
}